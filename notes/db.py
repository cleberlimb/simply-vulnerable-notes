import os
import mariadb
import sqlite3

from notes import note, db_backend


def create_connection():
    conn = None
    db_name = os.environ.get("NOTES_DB_DATABASE")

    # Remove unsupported '-' from database name
    db_name = db_name.replace("-", "")
    
    if db_name is None:
        note.logger.info("No Database Name set, defaulting to 'default'")
        db_name = "default"
    
    if db_backend == 'mariadb':
        try:
            conn = mariadb.connect(
                user="root",
                password=os.environ.get("DB_ROOT_PWD"),
                host="mariadb",
                port=3306
            )

            try:
                query = "CREATE DATABASE IF NOT EXISTS %s"
                c = conn.cursor()
                c.execute(query, db_name)
            except Exception as e:
                note.logger.error("Error (MariaDB): cannot create database %s - %s" % (db_name, e))
                return

            conn.database = db_name
            conn.auto_reconnect = True

        except Exception as e:
            note.logger.error("Error (MariaDB): cannot connect to db - %s" % e)
            return

    elif db_backend == 'local':
        try:
            conn = sqlite3.connect(db_name + ".db")
        except Exception as e:
            note.logger.error("Error (SQLite): cannot connect to db - %s" % e)
            return

    return conn

def create_table(conn):
    try:
        c = conn.cursor()
        c.execute(note.config['CREATE_TABLE_QUERY'])
    except Exception as e:
        note.logger.error("Error: cannot create table - %s" % e)

    conn.close()

def create_note(conn, notes, ip_address, hostname, admin=False):
    cur = conn.cursor()
    note.logger.info("Adding Note '{}'".format(str(notes)))
    
    try:
        query = "INSERT INTO notes VALUES (%s, %s, %s, %s)"
        args = str(notes), str(ip_address), str(hostname), str(admin)
        cur.execute(query, args)
    except Exception as e:
        note.logger.error("Error: cannot create note - %s" % e)
        conn.close()
        raise

    lastRowId = cur.lastrowid
    conn.commit()
    conn.close()
    
    return lastRowId

def delete_note(conn, id, admin=False):
    cur = conn.cursor()
    note.logger.info("Deleting Note with id: %s", id)

    try:
        query = "DELETE FROM notes WHERE (secret is FALSE AND id = %s)" % id
        if admin:
            query = "DELETE FROM notes WHERE id = %s" % id
        cur.execute(query)
    except Exception as e:
        note.logger.error("Failed to delete note with id '': %s" % e)
    
    conn.commit()
    conn.close()

def select_note_by_id(conn, id=None, admin=False):
    cur = conn.cursor()

    try:
        query = "SELECT id, data FROM notes WHERE secret IS FALSE"
        if id:
            query = query + " AND id = %s" % id
        if admin:
            query = "SELECT id, data, ipaddress, hostname, secret FROM notes"
            if id:
                query = query + " WHERE id = %s" % id
        cur.execute(query)
        
    except Exception as e:
        note.logger.error("Error: cannot select note by id - %s" % e)

    allItems = cur.fetchall()
    conn.close()

    if len(allItems) == 0:
        return []

    return allItems